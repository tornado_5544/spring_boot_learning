package com.spring;

import org.springframework.web.bind.annotation.*;

@RestController
public class GreetingResource {
	
	@RequestMapping(value = "/welcome", method = RequestMethod.GET)
	public String welcome(String name) {
		return "Hi "+ name + "\n Welcome to Spring Boot Application.";
	}
	
	@RequestMapping(value = "/divide", method = RequestMethod.GET)
	public float divide(float numerator, float denominator) {
		return (numerator / denominator);
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public int add(int first, int second) {
		return (first + second);
	}
	
	@RequestMapping(value = "/subtract", method = RequestMethod.GET)
	public int subtract(int first, int second) {
		return Math.abs(first - second);
	}
	
	@RequestMapping(value = "/multiply", method = RequestMethod.GET)
	public int multiply(int first, int second) {
		return (first * second);
	}
}
